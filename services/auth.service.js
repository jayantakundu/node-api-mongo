const User = require('../models/user');

// Create and Save a new Note
exports.create = function (req) {
    // Create a User
    const user = new User({
        username: req.username || "Untitled User",
        password: req.password
    });

    // Save Note in the database
    user.save()
        .then(function(data) {
            return data;
        }).catch(function(err) {
            return false;
        });
};