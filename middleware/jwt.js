const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
	// check header or url parameters or post parameters for token
	var token = req.headers['authorization'];

	// decode token
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, 'superSecret', function (err, decoded) {
			if (err) {
				return res.status(403).send({
					message: 'token expired.'
				});
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;
				next();
			}
		});
	} else {
		return res.status(403).send({
			message: 'No token provided.'
		});
	}
};