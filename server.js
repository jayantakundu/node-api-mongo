const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

// create express app
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

// Configuring the database
const dbConfig = require('./config/config');
const mongoose = require('mongoose');
const passport = require('passport');

mongoose.Promise = global.Promise;

// fix errors
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
// Connecting to the database
mongoose.connect(dbConfig.url)
.then(function (){
    console.log("Successfully connected to the database");    
}).catch(function (err) {
    console.log('Could not connect to the database.');
    process.exit();
});

require('./routes/routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;