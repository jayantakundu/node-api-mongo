const User = require('../models/user');
const bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

// Create and Save a new Note
exports.create = function (req, res) {
    // Validate request
    if(!req.body.username || !req.body.password) {
        return res.status(400).send({
            message: "Please pass username and password."
        });
    }

    // Create a User
    const user = new User({
        username: req.body.username || "Untitled User",
        password: createHash(req.body.password)
    });

    // Save Note in the database
    user.save()
    .then(function(data) {
        res.send(data);
    }).catch(function(err) {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
        });
    });
};

// Find a single note with a noteId
exports.login = function (req, res) {
    User.findOne({
        username: req.body.username
    }).then(function(user) {
        if (!user) {
            res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
            // check if password matches
            if(isValidPassword(user, req.body.password)) {
                // create a token with only our given payload
                // we don't want to pass in the entire user since that has the password
                const payload = {
                    id: user.id,
                    name: user.username
                };
                var token = jwt.sign(payload, 'superSecret');

                // return the information including token as JSON
                res.json({success: true, token: token});
            } else {
                res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
            }
            
        }
    }).catch(function(err) {
        res.status(500).send({
            message: err.message || "Error retrieving note with id "
        });
    });
};

// Generates hash using bCrypt
var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

// isValidPassword check
var isValidPassword = function(user, password){
    return bCrypt.compareSync(password, user.password);
};

