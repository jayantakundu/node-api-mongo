const isAuthenticated = require('../middleware/jwt');
const noteController = require('../controllers/note.controller');
const userController = require('../controllers/user.controller');

module.exports = function (app) {

    // define a simple route
    app.get('/', function(req,res){
        res.json({"message": "Welcome to EasyNotes application."});
    });
    // user register
    app.post('/register', userController.create);

    // user login
    app.post('/login', userController.login);

    // Create a new Note
    app.post('/notes', isAuthenticated, noteController.create);

    // Retrieve all Notes
    app.get('/notes', isAuthenticated, noteController.findAll);

    // Retrieve a single Note with noteId
    app.get('/notes/:noteId', isAuthenticated, noteController.findOne);

    // Update a Note with noteId
    app.put('/notes/:noteId', isAuthenticated, noteController.update);

    // Delete a Note with noteId
    app.delete('/notes/:noteId', isAuthenticated, noteController.delete);
};